#define _GNU_SOURCE

#include "makea.h"

#include <assert.h>
#include <lapacke.h>
#include <math.h>
#include <search.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SEED_VAL 0

/**
 * Random integer in [min,max)
 */
int randInt(int min, int max)
{
  int range = max-min;
  assert(range > 0);
  int remainder = RAND_MAX % range;
  int rn;
  do {
    rn = rand();
  } while (rn >= RAND_MAX - remainder && (remainder < range-1));
  return rn % range + min;
}

/**
 * Random double in (0,1)
 */
double randUniformEE(void)
{
  int rn;
  do {
    rn = rand();
  } while (rn == 0 || rn == RAND_MAX); // Disallow 0 and 1
  return (double)(rn) / RAND_MAX;
}

struct mtx_ele
{
  int arow, acol;
  double aelt;
};

int mtx_ele_cmp(const void *va, const void *vb)
{
  const struct mtx_ele *a = va, *b = vb;
  if (a->arow != b->arow) return a->arow - b->arow;
  else return a->acol - b->acol;
}

// This kludge is necessary because twalk does not support arguments to action
static struct twalk_action_args_t {
  double *a;
  int *ia;
  int *ja;
  double *row_sums;
  int *nfill_row;
} twalk_action_args;
void twalk_action(const void *nodep, const VISIT which, const int depth)
{
  // Do not do anything unless which == postorder or leaf.
  switch (which) {
    case preorder:
      return;
      break;
    case endorder:
      return;
      break;
    case postorder:
    case leaf:
      break;
  }
  struct mtx_ele *ele = *(struct mtx_ele **)nodep;

  // This renaming to avoid insanity
  struct twalk_action_args_t t = twalk_action_args;
  // Insert element into a and ja
  t.ja[t.ia[ele->arow]+t.nfill_row[ele->arow]] = ele->acol;
  t.a[t.ia[ele->arow]+t.nfill_row[ele->arow]] = ele->aelt;
  t.nfill_row[ele->arow]++;
  t.row_sums[ele->arow] += ele->aelt;

  // Insert image element into a and ja
  t.ja[t.ia[ele->acol]+t.nfill_row[ele->acol]] = ele->arow;
  t.a[t.ia[ele->acol]+t.nfill_row[ele->acol]] = ele->aelt;
  t.nfill_row[ele->acol]++;
  t.row_sums[ele->acol] += ele->aelt;
}

void makea(int n, int nnz, double a[restrict nnz], int ia[restrict n+1],
           int ja[restrict nnz])
{
  assert((nnz >= n) && (nnz <= ((long)n)*n));
  // Non-zeros in non-diagonal
  const int nnz_nd = nnz-n;
  // nnz_nd must be divisible by two.
  assert(nnz_nd % 2 == 0);
  const int nnz_nd_hlf = nnz_nd / 2;

  // Set seed
  srand(SEED_VAL);

  // Row counts
  int *nnz_row = malloc(sizeof(int)*n);
  int *nfill_row = malloc(sizeof(int)*n);
  for (int i = 0; i < n; i++) {
    nnz_row[i] = 1; // One for diagonal
    nfill_row[i] = 1;
  }

  void *tree_root = NULL;

  // Fill full matrix
  for (int i = 0; i < nnz_nd_hlf; i++) {
    int a,b,r,c;
    do {
      a = randInt(0,n); b = randInt(0,n);
    } while (a == b); // Don't allow diagonal entries
    if (a < b) { // Insert into upper triangle; r < c
      r = a; c = b;
    } else {
      r = b; c = a;
    }
    // Attempt to insert a new element into the tree
    struct mtx_ele *new = malloc(sizeof(struct mtx_ele));
    new->arow = r;
    new->acol = c;
    struct mtx_ele **ins = tsearch(new, &tree_root, &mtx_ele_cmp);
    if (*ins == new) {
      // This value was added to the tree
      new->aelt = randUniformEE();
      // Update row counts
      nnz_row[r]++;
      nnz_row[c]++;
    } else {
      // Duplicate
      free(new);
      i--; // Discard this element and try again.
    }
  }
  double *row_sums = calloc(n, sizeof(double));

  // Fill ia
  ia[0] = 0;
  for (int i = 1; i < n+1; i++)
  {
    ia[i] = ia[i-1] + nnz_row[i-1];
  }
  assert(ia[n] == nnz);

  twalk_action_args.a = a;
  twalk_action_args.ia = ia;
  twalk_action_args.ja = ja;
  twalk_action_args.row_sums = row_sums;
  twalk_action_args.nfill_row = nfill_row;

  // Fill a and ja (and set row_sums)
  twalk(tree_root, &twalk_action);


  // Set diagonals to ensure matrix is strictly diagonally dominant (to ensure SPD)
  for (int r = 0; r < n; r++) {
    a[ia[r]] = fmax(sqrt(n), 1.5*row_sums[r]);
    ja[ia[r]] = r;
  }

  // Clean up
  free(row_sums);
  free(nfill_row);
  free(nnz_row);
  tdestroy(tree_root, free);
}

void expand_mtx(int n, const double a[restrict], const int ia[restrict n+1],
                const int ja[restrict], double a_full[restrict], bool row_maj)
{
  if (row_maj) {
    for (int r = 0; r < n; r++) {
      for (int i = ia[r]; i < ia[r+1]; i++) {
        a_full[n*r + ja[i]] = a[i];
      }
    }
  } else {
    for (int r = 0; r < n; r++) {
      for (int i = ia[r]; i < ia[r+1]; i++) {
        a_full[n*ja[i] + r] = a[i];
      }
    }
  }
}

void print_mtx(int n, const double a[], const int ia[n+1], const int ja[])
{
  double *a_full = calloc(n*n, sizeof(double)); // All entries zero unless set
  expand_mtx(n, a, ia, ja, a_full, true);
  // Print
  for (int r = 0; r < n; r++) {
    printf("[ ");
    for (int c = 0; c < n; c++) {
      printf("%8.3f", a_full[n*r+c]);
      if (c < n-1) {
        printf(", ");
      }
    }
    printf(" ]\n");
  }
  free(a_full);
}

void verify_spd(int n, const double a[restrict], const int ia[restrict n+1],
                const int ja[restrict])
{
  double *a_full = calloc(n*n, sizeof(double)); // All entries zero unless set
  expand_mtx(n, a, ia, ja, a_full, false);

  // Verify symmetric
  for (int r = 0; r < n; r++) {
    for (int c = r+1; c < n; c++) {
      assert(a_full[n*r+c] == a_full[n*c+r]);
    }
  }
  printf("Test: matrix is symmetric\n");

  // Verify PD by printing eigenvalues (using LAPACKE)
  int matrix_layout = LAPACK_COL_MAJOR;
  char jobz = 'N';
  char range = 'I';
  char uplo = 'U';
  lapack_int n_lp = n;
  lapack_int lda = n;
  double vl = -1.0;
  double vu = -1.0;
  lapack_int il = 1;
  lapack_int iu = 1;
  double abstol = LAPACK_dlamch((char*)("Safe minimum"));
  lapack_int m;
  double w[1];
  double *z = NULL;
  lapack_int ldz = 1;
  lapack_int *isuppz = NULL;
  assert(!LAPACKE_dsyevr( matrix_layout, jobz, range, uplo, n_lp, a_full, lda,
                          vl, vu, il, iu, abstol, &m, w, z, ldz, isuppz ));
  printf("Smallest eig: %g\n", w[0]);
  assert(w[0] > 0.0);

  free(a_full);
}
