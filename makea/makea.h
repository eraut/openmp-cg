#ifndef MAKEA_H
#define MAKEA_H

/**
 * Matrix is returned in compressed sparse row (CSR) layout
 * a: array of nonzeros
 * ia[i]: index in a of first element in row i (ia[n] = nnz)
 * ja[j]: column index of a[j]
 */
void makea(int n, int nnz, double a[restrict nnz], int ia[restrict n+1],
           int ja[restrict nnz]);

void verify_spd(int n, const double a[restrict], const int ia[restrict n+1],
                const int ja[restrict]);

void print_mtx(int n, const double a[], const int ia[n+1], const int ja[]);
#endif
