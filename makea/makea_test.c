#include "makea.h"

#include <stdio.h>
#include <stdlib.h>

int main() // All entries zero unless set
{
  const int n = 32;
  const int nnz = 64;
  double *a = malloc(sizeof(double)*nnz);
  int *ia = malloc(sizeof(int)*(n+1)),
      *ja = malloc(sizeof(int)*nnz);
  makea(n, nnz, a, ia, ja);
  // Something happened! Let's see what we got.
  //print_mtx(n, a, ia, ja);
  printf("%g %g %g %g %g\n", a[ia[0]], a[ia[1]], a[ia[2]], a[ia[3]], a[ia[4]]);

  // Clean up
  free(a); free(ia); free(ja);
  return 0;
}
