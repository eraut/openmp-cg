#include "makea.h"

#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <omp.h>

// Number of blocks (each dim)
#define NB 16
// Block size (each dim)
#define BS 131072
// Full matrix row size
#define NA (NB*BS)
// Num nonzeros
#define NNZ (4*NA)

/**
 * Operation: b <- A.x, where A = (a,ia,ja) is a matrix in CSR layout
 * The SpMV operation is split into NB tasks over the rows of the matrix.
 */
void spmv(const double a[restrict NNZ], const int ia[restrict NA+1],
          const int ja[restrict NNZ], const double x[restrict NA],
          double b[restrict NA], int *restrict x_sync_obj)
{
#ifdef ERDBG_spmv_print
  // Warning: non-threadsafe
  printf("===== Begin spmv =====\na =\n");
  print_mtx(NA, a, ia, ja);
  printf("x = [ ");
  for (int i = 0; i < NA; i++) {
    printf("%g", x[i]);
    if (i < NA-1) {
      printf(", ");
    }
  }
  printf(" ]\n");
#endif

  // Generate "dummy" tasks to express the dependence of the tasks below on the
  // entire x array (as input). This is a kludge because OpenMP doesn't allow
  // overlapping depends regions.
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: x[BS*ti:BS]) depend(inout: x_sync_obj[0])
    {
    }
  }

  // Generate tasks
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: x_sync_obj[0]) depend(out: b[BS*ti:BS])
    {
#ifdef ERDBG_tp
      printf("Begin spmv task %d\n", ti);
#endif
      for (int r = BS*ti; r < BS*(ti+1); r++) {
        b[r] = 0;
        for (int k = ia[r]; k < ia[r+1]; k++) {
          b[r] += a[k]*x[ja[k]];
        }
      }
#ifdef ERDBG_tp
      printf("End spmv task %d\n", ti);
#endif
    }
  }
#ifdef ERDBG_spmv_print
  printf("b = [ ");
  for (int i = 0; i < NA; i++) {
    printf("%g", b[i]);
    if (i < NA-1) {
      printf(", ");
    }
  }
  printf(" ]\n");
#endif
}

void dotprod(const double x[restrict NA], const double y[restrict NA],
             double result_part[restrict NB], double *restrict result)
{
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: x[ti*BS:BS], y[ti*BS:BS]) \
                 depend(out: result_part[ti])
    {
      result_part[ti] = 0.0;
      for (int i = BS*ti; i < BS*(ti+1); i++) {
        result_part[ti] += x[i]*y[i];
      }
    }
  }

  // Interestingly enough, we need a task just to zero the result. There might
  // be a better way to do this.
#pragma omp task depend(out: result[0])
  {
    result[0] = 0.0;
  }

  // Reduction tasks
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: result_part[ti]) depend(inout: result[0])
    {
      result[0] += result_part[ti];
    }
  }
}

void selfdotprod(const double x[restrict NA], double result_part[restrict NB],
                 double *restrict result)
{
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: x[ti*BS:BS]) depend(out: result_part[ti])
    {
      result_part[ti] = 0.0;
      for (int i = BS*ti; i < BS*(ti+1); i++) {
        result_part[ti] += x[i]*x[i];
      }
    }
  }

  // Interestingly enough, we need a task just to zero the result. There might
  // be a better way to do this.
#pragma omp task depend(out: result[0])
  {
    result[0] = 0.0;
  }

  // Reduction tasks
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: result_part[ti]) depend(inout: result[0])
    {
      result[0] += result_part[ti];
    }
  }
}

void daxpy(const double *restrict alpha, const double x[restrict NA],
           double y[restrict NA], bool neg)
{
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: alpha[0], x[ti*BS:BS]) depend(inout: y[ti*BS:BS])
    {
      double my_alpha = neg ? -(*alpha) : *alpha;
      for (int i = ti*BS; i < (ti+1)*BS; i++) {
        y[i] += my_alpha*x[i];
      }
    }
  }
}

void dxpay(const double *restrict alpha, const double x[restrict NA],
           double y[restrict NA])
{
  for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: alpha[0], x[ti*BS:BS]) depend(inout: y[ti*BS:BS])
    {
      for (int i = ti*BS; i < (ti+1)*BS; i++) {
        y[i] = x[i] + (*alpha)*y[i];
      }
    }
  }
}

/**
 * Solve system Ax = b using CG method
 */
int main()
{
  double *a = malloc(sizeof(double)*NNZ);
  int *ia = malloc(sizeof(int)*(NA+1));
  int *ja = malloc(sizeof(int)*NNZ);

  double *x = malloc(sizeof(double)*NA);
  double *b = malloc(sizeof(double)*NA);

  double *r = malloc(sizeof(double)*NA);
  double *p = malloc(sizeof(double)*NA);
  double *Ap = malloc(sizeof(double)*NA);

  // Note: all these are declared outside the single block below so that they
  // are still in scope when the block ends.
  int x_sync_obj, p_sync_obj;
  double rTr_part[NB], rTr_new_part[NB], pTAp_part[NB];
  double rTr, rTr_new, pTAp, alpha, beta;
#pragma omp parallel
#pragma omp single
  {
    /**
     * Create matrix for problem
     */
    makea(NA, NNZ, a, ia, ja);

#ifdef ERDBG_verify_spd
    /**
     * Verify that this matrix is SPD
     */
    verify_spd(NA, a, ia, ja);
#endif

    /**
     * A RHS vector (b) will be generated corresponding to a solution (x) of all
     * ones. This is the method used by HPCG.
     */
    for (int i = 0; i < NA; i++) {
      x[i] = 1.0;
    }

    // NOTE: This is the point in which tasks are introduced. After this, all
    // resources must be explicly declared in a depend statement.
    spmv(a, ia, ja, x, b, &x_sync_obj);

    // Reset code
    for (int ti = 0; ti < NB; ti++) {
#pragma omp task depend(in: b[ti*BS:BS]) depend(out: x[ti*BS:BS], p[ti*BS:BS], \
            r[ti*BS:BS])
      {
#ifdef ERDBG_tp
        printf("Begin reset task %d\n", ti);
#endif
        // Reset x to 0 as "initial guess"
        for (int i = BS*ti; i < BS*(ti+1); i++) {
          x[i] = 0.0;
        }

        /**
         * p = b
         * r = b
         */
        for (int i = BS*ti; i < BS*(ti+1); i++) {
          p[i] = b[i];
          r[i] = b[i];
        }
#ifdef ERDBG_tp
        printf("End reset task %d\n", ti);
#endif
      }
    }

    selfdotprod(r, rTr_part, &rTr);

#pragma omp task depend(in: rTr)
    {
      printf("rTr = %g\n", rTr);
    }   
    const double tol = sqrt(NA)*DBL_EPSILON;
    const int MAX_ITER = 50;
    for (int it = 0; it < MAX_ITER; it++) {
      printf("Begin iteration %d\n", it);

      // Ap
      spmv(a, ia, ja, p, Ap, &p_sync_obj);

      // pTAp
      dotprod(p, Ap, pTAp_part, &pTAp);

#pragma omp task depend(in: rTr, pTAp) depend(out: alpha)
      {
        alpha = rTr / pTAp;
      }

      // r new
      daxpy(&alpha, Ap, r, true);

      // x new
      daxpy(&alpha, p, x, false);

      selfdotprod(r, rTr_new_part, &rTr_new);

      // This is a synchronization point!
#pragma omp taskwait

      printf("rTr_new = %g\n", rTr_new);
      if (rTr_new <= tol) {
        printf("Converged!\n");
        break;
      } else {
        // Prepare for next iteration
#pragma omp task depend(in: rTr_new) depend(inout: rTr) depend(out: beta)
        {
          beta = rTr_new / rTr;
          rTr = rTr_new;
        }
        dxpay(&beta, r, p);
      }
    }
  }
  printf("Done.\n");

  // Cleanup
  free(a);
  free(ia);
  free(ja);

  free(x);
  free(b);

  free(r);
  free(p);
  free(Ap);
}
